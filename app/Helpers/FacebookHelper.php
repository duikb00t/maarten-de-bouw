<?php 

namespace App\Helpers;

/**
 * This class will fetch all Facebook images from
 * a specific Facebook Page.
 */
class FacebookHelper {

    public static function getFacebookPageImages() {
        
      $fb = \App::make('SammyK\LaravelFacebookSdk\LaravelFacebookSdk');
      
      try {
          $response = $fb->get(
            Config::get('app.facebook_group') . '/?fields=albums{photos{source}}', 
            Config::get('app.facebook_token')
          );

        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
          dd($e->getMessage());
        }

        $object = $response->getGraphObject()->getField('albums');
        $images = [];
        if($object) {
          foreach($object  as $o) {
            foreach ($o['photos'] as $img ) {
              $images[] = $img['source'];
            }
          }
        }
        return $images;
	}
}
