<?php

namespace App\Jobs;

use App;    
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FetchFacbookImages extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $fb;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
     $fb = App::make('SammyK\LaravelFacebookSdk\LaravelFacebookSdk');


      try {
          $response = $fb->get(
            // ID?fields=albums{photos{source}}
            '623894591100913/?fields=albums{photos{source}}', 
            '972531656226962|jErfHNwAr4ewQNfXHenkD99Gq0k'
            );
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
          dd($e->getMessage());
        }

        $object = $response->getGraphObject()->getField('albums');

        $images = [];
        foreach($object  as $o) {
          foreach ($o['photos'] as $img ) {
            $images[] = $img['source'];
          }
        }
        
        return $images;

    }
}
