<?php

use App\Helpers\FacebookHelper;

Route::get('/', function () {
      $images = FacebookHelper::getFacebookPageImages();
      return view('welcome')->with('images', $images);
});

Route::get('gallery', 'GalleryController@index');