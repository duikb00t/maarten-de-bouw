var elixir = require('laravel-elixir');

elixir(function(mix) {

	// Images
	mix.copy('./node_modules/lightbox2/dist/images/*', 'public/images');

	// Scripts
    mix.scripts([
    	'./node_modules/jquery/dist/jquery.min.js',
    	'./node_modules/lightbox2/dist/js/lightbox.min.js',
    	'./node_modules/imagesloaded/imagesloaded.pkgd.min.js'
	]);

	// Markup SCSS/CSS assets
    mix.sass('app.scss');

});