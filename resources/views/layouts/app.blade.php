<!DOCTYPE html>
<html>
    <head>
        <title>{{ Config::get('app.sitename') }}</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

 
    </head>
    <body>
    <div id="border_left"></div>
    <div id="border_right"></div>
    <div id="border_bottom"></div>
    <header>
        <div class="logo">
            <h1>
                <a href="#">Maarten De Bouw</a>
            </h1>

            <span>Professional Photographer</span>
        </div>
       
        <nav>   
            <ul>
                <li><a href="#">HOME</a></li>
                <li><a href="#">CONTACT</a></li>
            </ul>
        </nav>
        <div class="clear"></div>
    </header>
        <script src="/js/all.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script type="text/javascript">
            
            $(function(){
                
                var $closeOverlay = $('#overlay a');
                $closeOverlay.on('click', function(e){
                    e.preventDefault();
                    $(this).closest('div').addClass('hidden');
                });

            });
        </script>
    </body>
</html>
