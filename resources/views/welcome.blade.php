<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <link rel="stylesheet" type="text/css" href="css/app.css">
        
    </head>
    <body>
    <div id="border_left"></div>
    <div id="border_right"></div>
    <div id="border_bottom"></div>
    <header>
        <div class="logo">
            <h1>
                <a href="#">Maarten De Bouw</a>
            </h1>

            <span>Professional Photographer</span>
        </div>
       
        
        <nav>   
            <ul>
                <li><a href="#">HOME</a></li>
                <li><a href="#">CONTACT</a></li>
            </ul>
        </nav>
        <div class="clear"></div>
    </header>
  
        <!-- 
        <div class="overlay-content">
        <h5>Hey, My name is</h5>
        <h2>DIVA</h2>
        <p>My passion is to make incredible creative work for all kind  of people. I’m a Portfolio and Photography Wordpress Theme and I’ll help you to showcase your awesome work very easy.</p>
        <a href="#" class="show-me">
        <i class="fa fa-eye"></i>
        Ok, Show me                </a>
        </div>
        -->

        <div id="main-wrapper" class="container-fluid">

         <div id="overlay">
            
            <h3>Welcome my name is</h3>
            <h2>Maarten De Bouw</h2>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque optio vel dignissimos repudiandae recusandae minima ipsum voluptates sit aut aliquam temporibus et similique debitis deleniti, quod. Mollitia sint molestias incidunt.</p>

            <a href="#">Show gallery</a>
        </div>
           
            <div class="col-25">
                @if(!empty($images))
                    @foreach ($images as $image)
                    <div>
                        <img src="{{ $image }}" alt="Test Images">
                    </div>
                    @endforeach
                @endif
            </div>


        </div>
        
        <script src="/js/all.js"></script>
        
        <script type="text/javascript">
            
            $(document).ready(function() {

                /* Images Loaded */
                $('.col-md-25').imagesLoaded( function() {
                  // images have loaded
                  console.log('loaded');
                });


                var $closeOverlay = $('#overlay a');
                $closeOverlay.on('click', function(e){
                    e.preventDefault();
                    $(this).closest('div').addClass('hidden');
                });
            });
        </script>
    </body>
</html>
